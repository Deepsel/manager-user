import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {User} from './models/User';

@Injectable()
export class DatabaseService {

  constructor(private http: HttpClient) {

  }

  getData(): Observable<Array<User>> {
    return this.http.get<Array<User>>('https://jsonplaceholder.typicode.com/users');
  }


  addUser(user: User): Observable<any> {
    const params = new HttpParams().set('name' , 'roman').set('email', 'user@mail.ru');
    return this.http.post<any>('https://jsonplaceholder.typicode.com/users', user, {params});
  }
}
