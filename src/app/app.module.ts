import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {AppComponent} from './app.component';
import {UserFormComponent} from './user-form/user-form.component';
import {UserManagerComponent} from './user-manager/user-manager.component';
import {UserDetailComponent} from './user-detail/user-detail.component';
import {DatabaseService} from './database.service';
import {HttpClient, HttpClientModule} from '@angular/common/http';

@NgModule({
  imports: [BrowserModule, FormsModule, HttpClientModule],
  declarations: [AppComponent, UserFormComponent, UserManagerComponent, UserDetailComponent],
  bootstrap: [AppComponent],
  providers: [DatabaseService]
})
export class AppModule {
}
