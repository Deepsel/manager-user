import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {User} from '../models/User';
import {UserService} from '../user-manager/user.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  @Input() user: User;
  @Output() deleteEvent = new EventEmitter<User>();

  constructor(public userService: UserService) {
  }

  ngOnInit() {

  }

  deleteUser(user: User) {
    this.deleteEvent.emit(user);
  }
}
