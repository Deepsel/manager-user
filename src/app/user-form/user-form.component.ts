import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {User} from '../models/User';
import {UserService} from '../user-manager/user.service';
import {DatabaseService} from '../database.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

  user: User;
  userName: string;
  userEmail: string;
  @Output() createUserEvent = new EventEmitter<User>();

  constructor(public userService: UserService, public databaseService: DatabaseService) {
  }

  ngOnInit() {

  }

  createUser() {
    this.userService.updateList(new User(this.userName, this.userEmail));
    this.databaseService.addUser(new User(this.userName, this.userEmail)).subscribe((data) => {
          console.log(data);
    });
  }


}
