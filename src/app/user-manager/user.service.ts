import {User} from '../models/User';

export class UserService {

  user: User;
  users: User[] = [{name: 'Roman', email: 'roman@email.com'}];
  selectedUser: User;


  updateList(user: User) {
    this.users.push(user);
  }

  selectUser(user: User) {
    this.selectedUser = user;
  }


}
