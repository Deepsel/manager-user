import {Component, OnInit} from '@angular/core';
import {UserService} from './user.service';
import {DatabaseService} from '../database.service';
import {User} from '../models/User';


@Component({
  selector: 'app-user-manager',
  templateUrl: './user-manager.component.html',
  styleUrls: ['./user-manager.component.css'],
  providers: [UserService]
})
export class UserManagerComponent implements OnInit {

  selectedUser: User;
  users: User[];

  constructor(public userService: UserService, public databaseService: DatabaseService) {

  }

  ngOnInit() {
    this.selectedUser = this.userService.selectedUser;
    this.users = this.userService.users;
  }

  selectUser(user: User) {
    this.selectedUser = this.userService.selectedUser;
    this.userService.selectUser(user);
  }

  deleteUser(user: User) {
    this.users.splice(this.users.indexOf(user), 1);
    this.selectedUser = null;
  }

  showData() {
    this.databaseService.getData().subscribe(data => {
      this.users = data;
    });
  }
}
